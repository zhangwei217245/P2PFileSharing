# Environment Requirement:

* JDK 8 U74
* MAVEN 3.3.3

# P2PFileSharing

## Compile Both Indexing Server and Peer Artifacts

```
mvn clean package
```

```math
\frac{a}{b}
```

## P2PIndexingServer

### Run:

```
cd target
tar zxvf indexing-server-bin.tar.gz
cd indexing-server/bin
./indexing-server -ip <external ip> -port <port number>
```

## P2PPeer

### Run:

```
cd target
tar zxvf peer-bin.tar.gz
cd peer/bin
./peer -ip <external ip> -port <port number>
```

