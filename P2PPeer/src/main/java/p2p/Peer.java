package p2p;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author cuiyuan
 */
public interface Peer {

    public void registerInstance(String indexingServerAddress);
    
    public List<String> getFileNameList();
    
    public String searchFile(String fileName);
    /**
     * download file with fileName from src.
     * @param src
     * @param fileName 
     */
    public void downLoadFile(String src, String fileName);

    /**
     * write file to servlet response
     * @param fileName
     * @param resp
     * @param sc
     * @return
     * @throws FileNotFoundException
     */
    public File writeFileToResponse(String fileName, HttpServletResponse resp, ServletContext sc) throws FileNotFoundException;

    public boolean isFileLocallyStored(String fileName);

}
