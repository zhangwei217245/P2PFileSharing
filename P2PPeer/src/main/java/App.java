import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;
import p2p.Peer;
import p2p.PeerImpl;
import servlet.PeerServlet;
import util.ConsoleUtils;
import util.PeerConfig;

import javax.servlet.annotation.WebServlet;
import java.io.File;

/**
 * Created by cuiyuan on 3/17/16.
 *
 * Initializing Tomcat from Normal App
 *
 * https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat
 *
 */
public class App {

    static Options options = new Options();
    private static Tomcat tomcat = null;

    private static void buildOptions() {
        // build option tables

        options.addOption(new Option("help", "print this message"));

        options.addOption(Option.builder("port").hasArg()
                .desc("port number")
                .build());

        options.addOption(Option.builder("ip").hasArg()
                .desc("external ip address")
                .build());
    }


    public static String[] parseArgs(String[] args) {
        String[] rst = new String[2];
        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("peer", options);
                System.exit(0);
            }

            if (line.hasOption("ip")) {
                rst[0] = line.getOptionValue("ip");
            } else {
                throw new ParseException("argument 'ip' is required.");
            }


            if (line.hasOption("port")) {
                rst[1] = line.getOptionValue("port");
            } else {
                throw new ParseException("argument 'port' is required.");
            }

        } catch (ParseException exp) {
            System.out.println("Arguments Error:" + exp.getMessage());
            System.exit(-1);
        }
        return rst;
    }

    public static void main(String[] args) throws Exception {

        PeerConfig config = PeerConfig.getInstance();

        String indexingServer = ConsoleUtils.readString("Please input indexing server address (in the form of \"http://ip:port/\"):");
        config.getIndexingServerAddresses().add(indexingServer);

        String sharedDir = ConsoleUtils.readString("Please input the path for shared directory:");
        config.setSharedDirPath(sharedDir);

        buildOptions();
        //The port that we should run on can be set into an environment variable
        //Look for that variable and default to 8080 if it isn't there.
        String[] argValues = parseArgs(args);
        config.setIp_addr(argValues[0]);
        config.setPortNum(Integer.valueOf(argValues[1]));

        PeerImpl.getInstance().registerInstance(config.getIndexingServerAddresses().get(0));

        String appHome = System.getProperty("app.home");
        String webappDirLocation = appHome + "/webapp/";
        tomcat = new Tomcat();
        tomcat.setPort(config.getPortNum());

        StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(webappDirLocation).getPath());
        System.out.println("configuring app with basedir: " + new File(webappDirLocation).getPath());

        // Declare an alternative location for your "WEB-INF/classes" dir
        // Servlet 3.0 annotation will work
        // But this one is using another class loader, causing two different class context.
//        File additionWebInfClasses = new File(appHome + "/classes");
//        WebResourceRoot resources = new StandardRoot(ctx);
//
//        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
//                additionWebInfClasses.getAbsolutePath(), "/"));
//        ctx.setResources(resources);

        String servletName = PeerServlet.class.getAnnotation(WebServlet.class).name();
        String servletClass = PeerServlet.class.getName();

        tomcat.addServlet(ctx, servletName, servletClass );
        for (String urlPattern : PeerServlet.class.getAnnotation(WebServlet.class).urlPatterns()
             ) {
            ctx.addServletMapping(urlPattern, servletName);
        }


        String peerAddr = String.format("http://%s:%s", config.getIp_addr(), config.getPortNum());
        System.out.println("======================== NOTICE =====================");
        System.out.println(String.format("Peer Initiated at %s\n" +
                "To download a file, send post request to %s/file with parameter 'filename'", peerAddr, peerAddr));
        System.out.println("======================== NOTICE =====================");

        tomcat.start();

        new Thread(() -> {
            Peer peer = PeerImpl.getInstance();

            while (true) {
                String fileName = ConsoleUtils.readString("Please input the file you want to download : ");

                try {
                    if (fileName != null && fileName.length() > 0) {
                        if (peer.isFileLocallyStored(fileName)) {
                            System.out.println(String.format("This file already exists in %s", config.getSharedDirPath()));
                        } else {
                            String targetPeer = peer.searchFile(fileName);
                            if (StringUtils.isNotBlank(targetPeer)) {
                                System.out.println(String.format("Downloading file %s from %s ...", fileName, targetPeer));
                                peer.downLoadFile(targetPeer, fileName);
                            } else {
                                System.out.println(String.format("File %s cannot be found at any peer!", fileName));
                            }
                        }
                    } else {
                        System.out.println("Please input valid file name!");
                    }
                    Thread.sleep(1000);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

        }).start();


        tomcat.getServer().await();
    }

}
