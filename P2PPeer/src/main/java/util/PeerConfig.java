package util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuiyuan on 3/18/16.
 */
public class PeerConfig {

    private int portNum = 8080;

    private String ip_addr = "127.0.0.1";

    private List<String> indexingServerAddresses = new ArrayList<>();

    private String sharedDirPath;// = System.getProperty("java.tmp.io");

    private static PeerConfig instance = new PeerConfig();

    public static PeerConfig getInstance() {return instance; }

    private PeerConfig(){}

    public String getIp_addr() {
        return ip_addr;
    }

    public void setIp_addr(String ip_addr) {
        this.ip_addr = ip_addr;
    }

    public int getPortNum() {
        return portNum;
    }

    public void setPortNum(int portNum) {
        this.portNum = portNum;
    }

    public String getSharedDirPath() {
        return sharedDirPath;
    }

    public void setSharedDirPath(String sharedDirPath) {
        this.sharedDirPath = sharedDirPath;
    }

    public List<String> getIndexingServerAddresses() {
        return indexingServerAddresses;
    }
}
